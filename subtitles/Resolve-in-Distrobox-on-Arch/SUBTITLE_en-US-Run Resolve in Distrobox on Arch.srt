1
00:00:00,000 --> 00:00:02,116
Hello everybody, this is Drew Naylor.

2
00:00:02,516 --> 00:00:04,700
Today I'm going to
be showing you how to run

3
00:00:05,000 --> 00:00:07,116
DaVinci Resolve on

4
00:00:10,099 --> 00:00:13,833
EndeavourOS, or Arch
Linux, but I'm running EndeavourOS.

5
00:00:14,966 --> 00:00:18,183
So this was really, really confusing,

6
00:00:18,949 --> 00:00:20,250
And, um,

7
00:00:22,366 --> 00:00:25,033
basically, I am using

8
00:00:26,383 --> 00:00:28,033
distrobox for this.

9
00:00:28,600 --> 00:00:33,016
So, uh, what I've read online,

10
00:00:33,433 --> 00:00:36,916
I did a lot of searching, I found a
lot of stuff about running it on Fedora.

11
00:00:37,750 --> 00:00:40,366
There's a lot of YouTube
videos about this you can find.

12
00:00:41,116 --> 00:00:43,766
Also, uh,

13
00:00:46,899 --> 00:00:48,416
also, um,

14
00:00:50,799 --> 00:00:53,466
yeah, there's a lot
of different things here.

15
00:00:54,649 --> 00:00:58,166
There's, there's two
repositories that I found.

16
00:00:58,616 --> 00:01:00,399
Let me find, try to find them.

17
00:01:00,600 --> 00:01:04,866
Uh, oh, there's
Resolve Flatpak, I didn't do that.

18
00:01:05,716 --> 00:01:08,783
Because of apparent issues.

19
00:01:09,133 --> 00:01:09,866
Oh, that's the wrong one.

20
00:01:10,283 --> 00:01:14,599
Issues in the,
because of the repo's Issues,

21
00:01:14,599 --> 00:01:17,799
this doesn't really sound like I
want it, something I want to deal with.

22
00:01:18,500 --> 00:01:21,283
Um, oh wait that's distrobox, hang on.

23
00:01:22,700 --> 00:01:26,466
Ah, this, Resolve by, um,

24
00:01:26,833 --> 00:01:28,566
this Resolve repository,

25
00:01:29,716 --> 00:01:31,916
for container scripts to build and run

26
00:01:31,916 --> 00:01:35,466
DaVinci Resolve Studio
for Linux using Docker or podman.

27
00:01:36,283 --> 00:01:39,666
I tried these, I
tried this, they didn't work.

28
00:01:40,183 --> 00:01:44,799
There's also, I'll leave
links to all these in the description.

29
00:01:45,533 --> 00:01:46,883
There's also DaVinciBox.

30
00:01:47,483 --> 00:01:48,700
I tried this too.

31
00:01:49,549 --> 00:01:50,799
It also didn't work.

32
00:01:51,666 --> 00:01:52,916
I have an Nvidia GPU.

33
00:01:54,116 --> 00:02:00,216
Uh, so, um, so that's kind of important.

34
00:02:01,116 --> 00:02:04,766
So, uh, what I did, what I figured out,

35
00:02:05,000 --> 00:02:09,266
is I found this, I
found, I eventually found

36
00:02:09,266 --> 00:02:13,933
this post here, this
comment on this post here.

37
00:02:14,716 --> 00:02:16,866
Um, on Reddit.

38
00:02:16,866 --> 00:02:20,133
And by rscmcl,

39
00:02:20,966 --> 00:02:22,583
and they said that
they just got it working

40
00:02:22,583 --> 00:02:24,983
yesterday using
distrobox without any issue.

41
00:02:25,483 --> 00:02:27,433
They took notes with
it, and they said that they

42
00:02:27,433 --> 00:02:30,016
have a laptop with an
Nvidia card with CUDA installed

43
00:02:30,016 --> 00:02:33,099
from rpmfusion
running Fedora Silverblue.

44
00:02:33,916 --> 00:02:38,483
Obviously, I am not
running Fedora, I'm running EndeavourOS.

45
00:02:40,766 --> 00:02:45,583
So, um, what we're
going to be doing in this video,

46
00:02:45,583 --> 00:02:47,400
I'm going to close DaVinci Resolve.

47
00:02:49,466 --> 00:02:50,900
I don't need it running right now.

48
00:02:51,483 --> 00:02:54,116
Uh, what I'm going to do is exit this,

49
00:02:55,283 --> 00:02:58,566
and then I have, I have these notes here.

50
00:02:59,883 --> 00:03:04,300
Uh, so, there's some, all the
links are going to be in the description.

51
00:03:05,366 --> 00:03:09,199
Um, so you might need, so you'll need to

52
00:03:09,199 --> 00:03:11,699
install distrobox
and podman at the very least.

53
00:03:12,433 --> 00:03:19,400
You should probably go through the
list of requirements here, let me find it.

54
00:03:19,933 --> 00:03:24,316
You'll probably need podman,
you'll need podman, probably fuse-overlayfs.

55
00:03:25,816 --> 00:03:27,766
I'm pretty sure
you'll need both of those.

56
00:03:28,316 --> 00:03:32,883
And then you'll need distrobox.

57
00:03:34,066 --> 00:03:35,933
Let me try to find the
other one. Where's the other one?

58
00:03:37,433 --> 00:03:40,533
Uh, ah here's DaVinciBox.

59
00:03:41,550 --> 00:03:46,783
Uh, okay, so when I installed
EndeavourOS, I used the Nvidia install option.

60
00:03:47,833 --> 00:03:49,283
Made sure that was installed.

61
00:03:49,883 --> 00:03:53,033
Um, you had podman
and I'm using distrobox,

62
00:03:53,716 --> 00:03:57,400
and you'll need to download
Resolve from Blackmagic's website.

63
00:03:58,266 --> 00:04:01,683
Uh, okay, so, I'm
just going to go into it now.

64
00:04:02,416 --> 00:04:07,266
So, they got it
working there, and then, um,

65
00:04:08,083 --> 00:04:09,983
you'll need to
create a distrobox container.

66
00:04:10,166 --> 00:04:14,466
And note by me that you
can't use "--nvidia" here

67
00:04:14,916 --> 00:04:17,483
if you're using,
like, Arch Linux or EndeavourOS,

68
00:04:18,133 --> 00:04:20,566
or won't be able to get
your GPU running in Resolve.

69
00:04:20,983 --> 00:04:25,233
Now, use a GTX 1060 6GB SC, in case

70
00:04:25,233 --> 00:04:27,616
you use something
else, if it doesn't work.

71
00:04:28,850 --> 00:04:30,933
It might work, it
might not, I don't know.

72
00:04:31,600 --> 00:04:33,783
And you'll need the rpmfusion drivers.

73
00:04:34,866 --> 00:04:38,733
And this command here
might work on a Fedora host.

74
00:04:38,733 --> 00:04:44,666
So, what we're going to do is
we're going to copy this command here.
(sorry I didn't read it out, I was tired)

75
00:04:45,666 --> 00:04:48,899
Copy, put it into this terminal.

76
00:04:50,699 --> 00:04:54,416
Um, and then give it a name here.

77
00:04:56,000 --> 00:05:00,433
Um, resolve-example.

78
00:05:01,550 --> 00:05:02,949
But we're not done yet.

79
00:05:02,949 --> 00:05:06,866
We need to go to the
end here and do "--root".

80
00:05:08,583 --> 00:05:13,716
Okay, so we need to do "--root"
on the end to ensure that it will be able to

81
00:05:13,716 --> 00:05:16,899
run with root permissions and get the

82
00:05:16,899 --> 00:05:19,433
license dongle if
you're using the Studio version.

83
00:05:19,899 --> 00:05:23,899
If you're not using the Studio
version, I don't think you need to deal with that.

84
00:05:24,333 --> 00:05:28,983
But, you'll, if you're
using the dongle, you will.

85
00:05:29,016 --> 00:05:33,133
So we can just do this, do a password.

86
00:05:34,250 --> 00:05:36,000
Okay, I'm doing the user password.

87
00:05:36,483 --> 00:05:38,166
Now we need to do distrobox

88
00:05:40,433 --> 00:05:41,000
enter

89
00:05:42,833 --> 00:05:43,633
--root

90
00:05:45,766 --> 00:05:46,983
resolve-example.

91
00:05:48,583 --> 00:05:50,333
Dash, resolve dash example.

92
00:05:52,683 --> 00:05:55,833
And then it starts the
container and installs basic packages.

93
00:05:55,833 --> 00:05:56,983
I'm going to trim this out.

94
00:05:57,000 --> 00:05:59,216
It'll take a little while.

95
00:05:59,533 --> 00:06:04,416
Okay, now that that's done installing
the basic packages and all this stuff here,

96
00:06:04,416 --> 00:06:09,583
we need to have a
password for running as root.

97
00:06:10,149 --> 00:06:14,416
Do not set a different
password from your user account's password.

98
00:06:14,633 --> 00:06:19,033
Your actual user account's
password if you're using rootful distrobox.

99
00:06:19,750 --> 00:06:23,566
It will break your password
and you won't notice until later.

100
00:06:23,933 --> 00:06:24,666
It's not good.

101
00:06:25,316 --> 00:06:29,883
So I'm just going to
do root because, oh, hang on.

102
00:06:30,133 --> 00:06:33,366
Agh! I can't just do
password. That's a shame.

103
00:06:33,616 --> 00:06:37,433
Ah! It did. One, two,
three, four, five, six, seven, eight.

104
00:06:38,233 --> 00:06:42,383
This is discriminatory
against ADHD video creators.

105
00:06:42,716 --> 00:06:46,100
What was my password? I
don't remember what my password is.

106
00:06:47,300 --> 00:06:49,833
Um, ah, hang on.

107
00:06:51,000 --> 00:06:52,183
Okay, let's try that again.

108
00:06:52,649 --> 00:07:02,699
Now that I got the password remembered okay and
something simple that I won't forget easily.

109
00:07:03,399 --> 00:07:10,483
We can, um, go ahead
and copy this command here.

110
00:07:11,399 --> 00:07:14,016
We'll need to install
these packages in the container.

111
00:07:14,800 --> 00:07:20,899
So sudo dnf install all these packages.

112
00:07:22,783 --> 00:07:26,800
And then you'll want to do yes.
You can look over these if you want, but.

113
00:07:28,899 --> 00:07:32,699
Um, and this will probably
take a while. I'll just speed it up.

114
00:07:34,816 --> 00:07:36,333
Okay, it's already done. Nevermind.

115
00:07:37,633 --> 00:07:42,766
And now it's just installing it
and I think it's, yeah, it's already done.

116
00:07:43,533 --> 00:07:44,716
Um, pretty much.

117
00:07:46,283 --> 00:07:47,566
Yeah, there we go. Okay.

118
00:07:48,266 --> 00:07:51,166
So now what we
need to do is go down here.

119
00:07:53,566 --> 00:07:56,233
Ah, we need to
install resolve in the container.

120
00:07:57,033 --> 00:07:59,899
So... move this down here.

121
00:08:01,000 --> 00:08:15,666
Um, so we need to cd into
Downloads, resolve, davincibox, DaVinci_Resolve...

122
00:08:16,016 --> 00:08:20,916
And then there's the
file in there and sudo ./

123
00:08:20,916 --> 00:08:27,716
DaVinci_Resolve_Studio_18.6.2_Linux.run, -i for interactive...

124
00:08:28,766 --> 00:08:33,000
And then it's asking us if we want
to install DaVinci Resolve on this computer.

125
00:08:33,399 --> 00:08:35,916
Yes. And if you agree to the terms and

126
00:08:35,916 --> 00:08:40,566
conditions, obviously I
do because I'm using Resolve.

127
00:08:41,250 --> 00:08:43,700
Yes. And now it's just going to install.

128
00:08:43,700 --> 00:08:45,466
This will take a
while. I'll just speed it up.

129
00:08:49,716 --> 00:08:54,616
Alright, now that that's done, we're
gonna need to fix some libraries in here.

130
00:08:54,616 --> 00:09:00,850
So I'm just gonna copy this, these
commands, paste in here. Please don't do

131
00:09:00,850 --> 00:09:05,333
this. Make sure you know what
you're doing rather than just copying and

132
00:09:05,350 --> 00:09:12,899
pasting random commands. But if
you run... like... yeah, look these over. I already

133
00:09:12,899 --> 00:09:18,716
looked them over. So I'm gonna do
that and it puts us into the libs folder.

134
00:09:19,450 --> 00:09:23,033
And now if we run resolve...

135
00:09:27,816 --> 00:09:29,433
Here's what it's gonna do.

136
00:09:31,450 --> 00:09:33,700
So it loads up, says unsupported GPU

137
00:09:33,700 --> 00:09:36,950
processing mode. We do
update configuration, go to

138
00:09:36,950 --> 00:09:41,833
memory and GPU. You'll see
there's no GPU here. Yeah, you'll not...

139
00:09:42,200 --> 00:09:48,016
You won't see any GPU. You can't see
it. And if you're using AAC files on Linux

140
00:09:48,200 --> 00:09:56,533
and Resolve, you can't hear it
either. So what we need to do to fix that is to

141
00:09:56,533 --> 00:10:12,399
add the rpmfusion repos here. And
then you'll then paste. Then these are from

142
00:10:12,433 --> 00:10:18,216
the rpmfusion mirrors, both the
free and non-free ones. And then push enter

143
00:10:18,216 --> 00:10:21,516
and it'll download them. Push yes, enter.

144
00:10:21,933 --> 00:10:24,316
Now it's
installed. And now we can install

145
00:10:24,316 --> 00:10:33,816
CUDA. So we copy this and paste.
Push enter. This will take a lot longer.

146
00:10:37,566 --> 00:10:44,799
Alright, yeah, 1.3 gigabytes.
Installed size, 448 megabytes. So push yes, enter.

147
00:10:45,833 --> 00:10:48,016
I'm gonna just speed
this up. This will be a while.

148
00:10:51,366 --> 00:10:52,100
Alright now

149
00:10:53,166 --> 00:10:58,799
They're asking if we want to
trust the non-free rpmfusion repo key

150
00:10:59,616 --> 00:11:04,233
79BD I already
checked that you should check

151
00:11:04,233 --> 00:11:07,766
it yourself, but I
already checked it. It's fine

152
00:11:09,899 --> 00:11:14,066
according to their website I
didn't do the GP- the full GPG check but

153
00:11:15,899 --> 00:11:18,149
Far as I know it's
fine. Now it's installing.

154
00:11:20,666 --> 00:11:21,666
This may take a while.

155
00:11:24,933 --> 00:11:28,633
Looks like it gets kind
of stuck here but it's, far as

156
00:11:28,633 --> 00:11:31,483
I know, it's okay. Just,
yeah, it just takes a while there.

157
00:11:33,149 --> 00:11:35,033
Alright now we can run Resolve again.

158
00:11:37,350 --> 00:11:39,916
And it's loading
this up. I think these are

159
00:11:39,916 --> 00:11:45,533
random pictures. And
now we can do untitled project.

160
00:11:47,033 --> 00:11:53,850
Ta-da! Or I can show you the other
project. Open this back up. Yeah they do random

161
00:11:55,049 --> 00:11:59,616
pictures. I didn't know that. And
then this is a test project I did earlier.

162
00:12:00,200 --> 00:12:01,850
Let's go to the
edit page. I don't like the

163
00:12:01,850 --> 00:12:06,916
Cut page. So yeah
this is just one I did earlier.

164
00:12:06,916 --> 00:12:09,250
(some noises and me saying
something I can't tell)

165
00:12:09,250 --> 00:12:14,600
You know, just to
test it. And you can go into

166
00:12:14,600 --> 00:12:19,850
here. And I don't
know how to use the default.

167
00:12:20,733 --> 00:12:27,566
Uh is this cut? No. How do I? How do I?

168
00:12:29,700 --> 00:12:32,766
Blade edit mode. There
we go. You can cut this up here.

169
00:12:34,966 --> 00:12:38,733
And then we go
back to here. And then move

170
00:12:38,733 --> 00:12:43,600
these over. Move this over. Go over here.

171
00:12:43,616 --> 00:12:52,833
Change the volume. I already unlinked
these clips. So yeah. And then you can like

172
00:12:54,816 --> 00:12:57,433
do all the stuff that
you can in Resolve. You know.

173
00:12:57,883 --> 00:13:01,250
(Testing again.) You know like all this.

174
00:13:01,600 --> 00:13:07,266
(Testing again. *dinging sounds*
This should be separate tracks.)
And then it does that and everything.

175
00:13:08,333 --> 00:13:09,533
Uh so.

176
00:13:12,500 --> 00:13:20,950
But the only issue is you won't have
a thing here. You might look like you do.

177
00:13:21,350 --> 00:13:23,299
But this isn't, this isn't the real one.

178
00:13:34,016 --> 00:13:38,966
Okay, so I managed
to fix my password and I

179
00:13:38,966 --> 00:13:42,433
needed to use a boot
disk, but I got that done.

180
00:13:42,899 --> 00:13:46,816
I was petting the cat for a while
and then I fixed my password and I'm back.

181
00:13:47,649 --> 00:13:52,133
So this is what I
managed to figure out with this.

182
00:13:52,500 --> 00:13:58,716
Um, oh not this. Um, but I made a run

183
00:13:58,716 --> 00:14:03,233
resolve.sh script, which
does the bin bash thing at the

184
00:14:03,266 --> 00:14:08,233
beginning and then
runs konsole -e because

185
00:14:08,233 --> 00:14:11,333
you need to type in
your password. You have to,

186
00:14:12,233 --> 00:14:15,333
like, you really need to use your main

187
00:14:15,333 --> 00:14:18,966
password, your actual
user account password. Otherwise,

188
00:14:18,966 --> 00:14:22,783
it can really mess
it up for some reason if

189
00:14:22,783 --> 00:14:25,399
you use a different
password for your rootful

190
00:14:26,250 --> 00:14:31,816
distrobox container. So konsole -e, that will allow you to type in a password.

191
00:14:33,283 --> 00:14:42,066
Um, and then runs 
/usr/bin/distrobox-enter --root and then your

192
00:14:43,733 --> 00:14:52,000
your container name and then dash
dash and then /usr/bin/run-resolve-for-real.sh.

193
00:14:52,799 --> 00:14:56,633
/usr/bin/run-resolve-for-real.sh is inside

194
00:14:56,633 --> 00:15:10,766
the container. It's
QT_QPA_PLATFORM=xcb (two underscores before GLX) __GLX_VENDOR_LIBRARY_NAME=nvidia /opt/resolve/bin/resolve

195
00:15:10,766 --> 00:15:14,633
and then you'll need
to do a chmod +x on this.

196
00:15:15,100 --> 00:15:18,783
See, chmod +x run-resolve-for-real.sh.

197
00:15:20,116 --> 00:15:23,566
Obviously you'll need
sudo too. I got this from,

198
00:15:24,383 --> 00:15:28,916
did I close it?
Yeah, I closed it. I got this

199
00:15:28,916 --> 00:15:32,500
from, I don't know
where I got this from. Oh yeah,

200
00:15:32,500 --> 00:15:36,000
I got this from
here and I modified after I

201
00:15:36,000 --> 00:15:39,799
modified this. I
added the glx vendor library

202
00:15:40,066 --> 00:15:44,283
thing in there and
otherwise it was just that just

203
00:15:44,283 --> 00:15:51,000
just this and um I
got the distrobox-enter thing

204
00:15:51,183 --> 00:16:02,033
from add defen- dev- add-davinci-launcher
down here. I got the idea for that from this,

205
00:16:02,033 --> 00:16:05,299
the original
version of this. It wasn't that

206
00:16:05,299 --> 00:16:08,216
but I modified it. I
think I showed that earlier

207
00:16:09,083 --> 00:16:13,316
but you get the idea.
So just to demonstrate, oh

208
00:16:13,316 --> 00:16:16,083
I closed it. Hang on.
Just to demonstrate double

209
00:16:16,166 --> 00:16:19,516
click on this,
execute, and then we type in the

210
00:16:19,516 --> 00:16:23,583
password and then it
runs Resolve and of course

211
00:16:23,583 --> 00:16:27,183
I don't have my USB
drive in there but I'll do

212
00:16:27,183 --> 00:16:30,716
that in a minute. Alright now it's plugged in.

213
00:16:31,016 --> 00:16:38,033
I'm gonna go back into here, type in
my password again. Okay and it should load it.

214
00:16:39,833 --> 00:16:43,799
Yeah, there there we go. So I hope this video

215
00:16:43,799 --> 00:16:48,866
helped. Sorry it's so
much longer than I expected. I

216
00:16:48,866 --> 00:16:56,166
hope I can trim
this down a lot. Yeah I had

217
00:16:56,166 --> 00:16:58,799
some issues with this
one but at least I learned so

218
00:16:58,799 --> 00:17:05,316
you won't have to.
Yeah goodbye for now. Oh some

219
00:17:05,316 --> 00:17:06,833
more details that I
can probably give about why

220
00:17:06,866 --> 00:17:13,916
we needed to do
rootful distrobox because down

221
00:17:13,916 --> 00:17:19,233
here, this resolve
thing here, it won't let you-

222
00:17:19,500 --> 00:17:21,616
yeah there's a
thing for Blackmagic Design

223
00:17:21,616 --> 00:17:26,683
UDEV rules but these
don't work to get your license

224
00:17:26,933 --> 00:17:32,083
license USB drive. I tried, it doesn't

225
00:17:32,083 --> 00:17:36,366
work so I wouldn't
waste your time with that.

226
00:17:37,333 --> 00:17:42,066
Unless it does work. I think it would
be better to use it if it does work otherwise

227
00:17:42,783 --> 00:17:46,533
if it doesn't work then hey there's my

228
00:17:46,533 --> 00:17:50,933
version of the way
to do it. Goodbye for now.

229
00:17:51,366 --> 00:17:56,616
And if you want a title
bar on Resolve in KDE in

230
00:17:56,616 --> 00:17:59,900
Kwin with Kwin. If
you're using Kwin and there's no

231
00:17:59,900 --> 00:18:03,466
title bar here as
you can probably tell, what

232
00:18:03,466 --> 00:18:06,716
you can do is you can
use this Kwin rule here on the

233
00:18:06,716 --> 00:18:11,866
Arch Wiki and you can
um and you can put that in

234
00:18:11,866 --> 00:18:15,683
that Kwin rule file
and uh you can copy and paste

235
00:18:15,766 --> 00:18:20,433
this into there and
then import it and then it'll

236
00:18:20,433 --> 00:18:23,333
it'll do that.
Something I also forgot to show is

237
00:18:23,333 --> 00:18:26,583
that if you go in
uh opening up Resolve- oh

238
00:18:26,583 --> 00:18:31,033
media offline why is
that? That's not good. Hang on.

239
00:18:31,483 --> 00:18:34,733
Okay I don't know
what's going on here. Oh wait

240
00:18:34,733 --> 00:18:39,000
are these just- hang
on. I think these are raw.

241
00:18:39,183 --> 00:18:46,000
Yeah hang on. Oh yeah
I know what's going on. Um

242
00:18:46,000 --> 00:18:49,883
anyway let's just bring
these in here and what you can do

243
00:18:49,883 --> 00:18:58,183
in Resolve is you can just go let me
take this- let me try to find a small one.

244
00:19:00,700 --> 00:19:01,933
2.6 gigs.

245
00:19:04,549 --> 00:19:07,666
Ah there we go.
There's a 125 megabyte one. Let's

246
00:19:07,666 --> 00:19:10,799
drag this over here
and you can just go like this.

247
00:19:12,266 --> 00:19:15,533
You can just drag it
right in there (me saying stuff in the timeline)
 and there you go.

248
00:19:18,066 --> 00:19:26,133
No need to go over here and do
import media and then browse for your media

249
00:19:27,183 --> 00:19:33,200
when you can just drag
it right in there. So I thought

250
00:19:33,200 --> 00:19:36,500
that was pretty cool. I
didn't know you could just do that

251
00:19:38,216 --> 00:19:42,000
but you can. I thought you didn't-
I thought you couldn't because there was

252
00:19:42,866 --> 00:19:45,316
some method of doing
it. I don't know which one

253
00:19:46,116 --> 00:19:50,433
that you can't do that
with but you can the way I did.

254
00:19:50,483 --> 00:20:01,033
So good. Also you'll need to install
nano to do the run-resolve-for-real script

255
00:20:02,616 --> 00:20:05,533
unless you want to use
another text editor in the container.

256
00:20:06,533 --> 00:20:10,716
Okay where is- oh did I
minimize it? Yeah goodbye for now.

