1
00:00:00,000 --> 00:00:01,600
Hello everybody, this is Drew Naylor.

2
00:00:02,233 --> 00:00:06,766
Today what I'm
doing is showing how I remove

3
00:00:06,766 --> 00:00:11,066
noises, like electrical noises
from my 3DS sound recordings

4
00:00:11,066 --> 00:00:18,233
when my 3DS is plugged into the wall
and the audio adapter is going to my camera.

5
00:00:18,666 --> 00:00:22,100
So what you'll need
to do is if you're using

6
00:00:22,100 --> 00:00:25,866
Premiere... Adobe
products like Premiere, you'll

7
00:00:25,883 --> 00:00:33,366
wanna do right click here on the
video clip, do "Edit Clip In Adobe Audition".

8
00:00:34,166 --> 00:00:36,500
That's gonna
open up Adobe Audition and

9
00:00:36,500 --> 00:00:39,133
then you'll want to
wait for it to load, select

10
00:00:39,133 --> 00:00:44,633
all, go to... this is
Audition CC 2017. (Go to) "Effects",

11
00:00:45,133 --> 00:00:48,333
"Noise Reduction/Restoration", "Noise Reduction (process)...",

12
00:00:48,333 --> 00:00:51,133
and then
you'll wanna click this

13
00:00:51,133 --> 00:00:54,083
folder here where
it says "Load a noise print

14
00:00:54,116 --> 00:00:54,716
from disk".

15
00:00:55,316 --> 00:00:57,433
If you already have one, that's great.

16
00:00:58,066 --> 00:01:00,200
What you wanna do is
like I have three of 'em here.

17
00:01:01,116 --> 00:01:06,483
So I'm gonna click on,
double click on this, click apply.

18
00:01:08,333 --> 00:01:11,566
wait for it, there
we go. And then I guess

19
00:01:11,566 --> 00:01:14,833
I should have
previewed it, but that's okay.

20
00:01:14,833 --> 00:01:17,450
I can show side by
side how it looks. So we

21
00:01:17,450 --> 00:01:21,466
wanna do Control+A again, "Effects", "Noise

22
00:01:21,500 --> 00:01:24,099
Reduction/Restoration", "Noise Reduction

23
00:01:24,099 --> 00:01:29,400
(process)...". Click the
other one (other file). Now I'll put these up

24
00:01:29,433 --> 00:01:31,250
for download. Click apply.

25
00:01:32,916 --> 00:01:39,783
Okay, so that's done. So then you
do it again, "Noise Reduction (process)..." for the

26
00:01:39,783 --> 00:01:43,683
third one, and then click apply.

27
00:01:47,150 --> 00:01:48,116
And then you hit save.

28
00:01:48,900 --> 00:01:50,583
It should be all good to go.

29
00:01:51,200 --> 00:01:52,900
And now it's generating the peak file.

30
00:01:53,433 --> 00:01:56,766
Okay, so in case
you don't already have FFT

31
00:01:56,766 --> 00:02:01,216
files for doing
noise removal, what you can

32
00:02:01,216 --> 00:02:09,283
do is you go into the
file and like find and look.

33
00:02:09,816 --> 00:02:11,133
So here it is zoomed out.

34
00:02:11,916 --> 00:02:15,183
So you look for
like these bands right here.

35
00:02:16,666 --> 00:02:21,366
Like these purple or
like these taller bands of stuff.

36
00:02:21,699 --> 00:02:27,500
And like here where I have zoomed
in right here, there's one right here.

37
00:02:28,633 --> 00:02:30,533
Right there where the hand is.

38
00:02:30,966 --> 00:02:32,883
So I'm gonna briefly play it for you.

39
00:02:32,900 --> 00:02:35,633
(calm 3DS eShop music with
trailing high-pitched whining noise)

40
00:02:36,199 --> 00:02:41,500
And you should have heard the
kind of a whining noise like, "Errr."

41
00:02:41,500 --> 00:02:44,616
I can't do it at the
right pitch (too tired), but you get the idea.

42
00:02:45,166 --> 00:02:52,849
So if we... oop! if we zoom in
here and we highlight this...

43
00:02:57,916 --> 00:03:01,133
As you can see,
there's nothing else there.

44
00:03:01,666 --> 00:03:06,533
You highlight
that, go up to "Effects", "Noise

45
00:03:06,533 --> 00:03:09,199
Reduction/Restoration", "Capture Noise Print".

46
00:03:10,566 --> 00:03:13,316
It says it will be
captured and loaded as a

47
00:03:13,316 --> 00:03:16,516
noise print for
use the next time the noise

48
00:03:16,533 --> 00:03:17,699
reduction effect is launched.

49
00:03:18,000 --> 00:03:18,616
Click okay.

50
00:03:19,716 --> 00:03:23,599
Go back to "Effects", "Noise
Reduction/Restoration", "Noise Reduction (process)...".

51
00:03:26,050 --> 00:03:30,466
And then you do "Capture Noise Print"
and it will grab the noise print from there.

52
00:03:31,250 --> 00:03:39,416
And then if we do select entire
file, then you do apply and wait for it.

53
00:03:42,166 --> 00:03:44,083
And now... Oh, it was right here.

54
00:03:44,883 --> 00:03:46,316
And now we can go here.

55
00:03:46,716 --> 00:03:49,500
(calm 3DS eShop music with no
trailing whining sound)

56
00:03:49,583 --> 00:03:50,283
And there's nothing.

57
00:03:51,633 --> 00:03:54,449
I'll play the first and
then the second so you can hear it.

58
00:03:54,449 --> 00:03:57,266
(calm 3DS eShop music with
trailing whining sound)

59
00:03:57,266 --> 00:04:00,066
(calm 3DS eShop music with no
trailing whining sound)

60
00:04:00,066 --> 00:04:02,550
You'll need to do
this for other stuff like here.

61
00:04:04,883 --> 00:04:05,266
And...

62
00:04:06,266 --> 00:04:08,483
Well, here is the same one, but...

63
00:04:09,816 --> 00:04:13,050
There's other
bands that you'll need to do.

64
00:04:13,966 --> 00:04:15,716
If you're... If you don't have...

65
00:04:17,433 --> 00:04:19,399
If you're not on 60 Hertz...

66
00:04:20,816 --> 00:04:22,166
Wall power frequency.

67
00:04:23,066 --> 00:04:25,449
You might need to do other...

68
00:04:27,666 --> 00:04:28,399
Do it yourself.

69
00:04:29,750 --> 00:04:31,666
My files might not work.

70
00:04:31,666 --> 00:04:33,216
They're under CC BY.

71
00:04:33,616 --> 00:04:35,166
Creative Commons By Attribution,

72
00:04:35,166 --> 00:04:39,550
so you can just use them
and then attribute them to me.

73
00:04:39,850 --> 00:04:41,666
Anyway, I hope that works.

74
00:04:41,883 --> 00:04:45,916
And if you need to
save one, you go to "Effects",

75
00:04:47,000 --> 00:04:48,583
"Noise Reduction/Restoration",

76
00:04:49,433 --> 00:04:50,716
"Noise Reduction (process)...".

77
00:04:52,183 --> 00:04:53,949
So it's the current noise print.

78
00:04:53,949 --> 00:04:55,649
And if you wanna
save it, you click here.

79
00:04:55,649 --> 00:04:56,816
"Save the current noise print".

80
00:04:57,883 --> 00:05:00,800
And then you can name it
whatever you want and then write it down.

81
00:05:01,899 --> 00:05:06,866
To see the
frequencies, you see over here Hertz.

82
00:05:07,600 --> 00:05:12,516
And you can drag up and
down for different Hertz frequencies.

83
00:05:15,483 --> 00:05:16,383
Into the kilohertz.

84
00:05:17,083 --> 00:05:19,466
Some things you might need to do.

85
00:05:20,300 --> 00:05:21,649
Might need to right click here.

86
00:05:23,866 --> 00:05:24,850
It might be notes.

87
00:05:25,666 --> 00:05:27,800
So you'll need to switch to Hertz.

88
00:05:29,550 --> 00:05:29,783
No.

89
00:05:30,250 --> 00:05:31,899
This isn't the defau- oh!

90
00:05:32,466 --> 00:05:33,983
This is how it usually looks.

91
00:05:34,583 --> 00:05:36,866
You'll need to click up here for the "Show

92
00:05:36,866 --> 00:05:39,966
Spectral Pitch
Display" to show it like this.

93
00:05:40,750 --> 00:05:43,016
So um, I hope that helps.

94
00:05:43,366 --> 00:05:44,016
Goodbye for now.

