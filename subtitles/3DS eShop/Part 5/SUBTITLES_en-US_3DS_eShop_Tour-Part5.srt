1
00:00:00,433 --> 00:00:06,400
Hello everybody, this is
Drew Naylor. Previously, we went through the um...

2
00:00:07,533 --> 00:00:12,199
I can't remember what it's called.
Account Activity. We went through this page.

3
00:00:14,833 --> 00:00:17,000
This thing here. We went through this.

4
00:00:19,199 --> 00:00:21,800
And now today we're
gonna go through redownalo...

5
00:00:22,533 --> 00:00:22,899
ahg

6
00:00:22,899 --> 00:00:27,000
Redownloadable
Software and then continue going on

7
00:00:27,000 --> 00:00:29,133
the Setting- going
through the Settings/Other

8
00:00:30,199 --> 00:00:31,100
part of the eShop.

9
00:00:32,833 --> 00:00:33,433
Um...

10
00:00:33,966 --> 00:00:34,166
Now...

11
00:00:35,266 --> 00:00:37,133
At the top it says Redownaloadable...

12
00:00:39,799 --> 00:00:40,866
Redownloadable Software.

13
00:00:41,666 --> 00:00:42,366
Select the up...

14
00:00:44,933 --> 00:00:48,600
Select the updated software you want
to download. Redownloadable Software: 4.

15
00:00:49,399 --> 00:00:51,066
This one's not applicable.

16
00:00:52,033 --> 00:00:56,266
This one's also not applicable. I'm
gonna guess these two are Netflix and YouTube.

17
00:00:57,466 --> 00:00:58,100
Give or take.

18
00:00:59,833 --> 00:01:00,366
And then...

19
00:01:01,200 --> 00:01:03,799
We have Dinosaur Office and Swapnote.

20
00:01:05,400 --> 00:01:06,700
Let's go into Dinosaur Office.

21
00:01:10,266 --> 00:01:13,266
Um... And then there's the
logo: Dinosaur Office with a

22
00:01:13,266 --> 00:01:17,733
backwards facing R at the
end. Backwards facing capital R.

23
00:01:18,466 --> 00:01:19,700
Title is Dinosaur Office.

24
00:01:20,799 --> 00:01:24,266
Downloadable Video. Release Date: to
be determined. And here's some screenshots.

25
00:01:25,166 --> 00:01:26,566
And the logo of Dinosaur Office.

26
00:01:29,466 --> 00:01:31,466
And the bottom
screen is some sort of pattern.

27
00:01:34,833 --> 00:01:36,433
Not entirely sure
what it's supposed to be.

28
00:01:37,433 --> 00:01:40,900
We've got dinosaurs with
party hats looking at the camera.

29
00:01:43,533 --> 00:01:46,500
And then we've got
some dinosaurs at a table.

30
00:01:47,833 --> 00:01:50,233
And the wall behind them is broken.

31
00:01:52,133 --> 00:01:53,266
Kind of like the um...

32
00:01:54,266 --> 00:01:55,033
The Kool-Aid Man...

33
00:01:56,333 --> 00:01:56,933
...ads.

34
00:02:00,433 --> 00:02:01,466
But... uh...

35
00:02:02,366 --> 00:02:05,433
Yeah, it's it's more
like on the Kool-Aid Man side

36
00:02:05,433 --> 00:02:08,000
of things than on the
Hbomberguy side of things.

37
00:02:10,233 --> 00:02:12,133
If you understand, if you know...

38
00:02:13,800 --> 00:02:14,800
Kind of what I mean by that.

39
00:02:15,966 --> 00:02:17,133
On the amount of...

40
00:02:18,133 --> 00:02:18,833
wall breakage.

41
00:02:19,633 --> 00:02:21,766
Dinosaur Office
publisher College Humor. Oh, I

42
00:02:21,766 --> 00:02:24,066
thought it was
Nintendo. I didn't know it would be them.

43
00:02:24,733 --> 00:02:28,533
Genre: other. Languages:
English, Spanish, French, and Portuguese.

44
00:02:31,166 --> 00:02:33,166
What type of person
is it suited for? Everyone:

45
00:02:33,166 --> 00:02:37,766
70%. What type of
game is it? Casual play: 65%.

46
00:02:38,199 --> 00:02:39,233
Related Genres: other.

47
00:02:43,699 --> 00:02:47,733
Swapnote. Software
Info. I already showed this before.

48
00:02:49,199 --> 00:02:52,300
We can redownload it from
there on the redownload screen.

49
00:02:53,333 --> 00:02:57,333
Yeah, I showed
that like two... maybe three

50
00:02:57,333 --> 00:03:00,199
videos ago? Your
downloads. I wonder what's in here.

51
00:03:02,066 --> 00:03:04,966
Oh! This is what I've
downloaded already. See a

52
00:03:04,966 --> 00:03:07,133
list of titles you have
downloaded. Downloaded Titles:

53
00:03:07,966 --> 00:03:08,566
15.

54
00:03:09,933 --> 00:03:11,366
My Nintendo Picross: Twilight Princess,

55
00:03:12,133 --> 00:03:15,133
Netflix. Oh, you can
redownload it from there.

56
00:03:18,199 --> 00:03:20,166
Interesting. That's weird.

57
00:03:20,233 --> 00:03:23,633
Ocarina of Time 3D.

58
00:03:24,500 --> 00:03:27,033
Mario and Luigi Dream
Team update. Mario and Luigi

59
00:03:27,033 --> 00:03:30,500
Dream Team. Flip Note
Studio 3D update available.

60
00:03:31,599 --> 00:03:32,933
Smash 3DS Special Demo.

61
00:03:33,699 --> 00:03:34,533
Zelda NES.

62
00:03:35,866 --> 00:03:39,166
Donkey Kong Country
Returns 3D. Photos with Mario.

63
00:03:39,166 --> 00:03:41,199
Oh, yeah, I remember
that's the icon that it displays.

64
00:03:42,333 --> 00:03:45,266
Four Swords
Anniversary Edition. Mario Land 2.

65
00:03:45,866 --> 00:03:48,300
YouTube. Oh, you can
redownload it from there.

66
00:03:49,333 --> 00:03:51,966
Dinosaur Office. Swapnote.

67
00:03:54,433 --> 00:03:54,866
That's it.

68
00:03:56,900 --> 00:03:57,766
Where is...

69
00:03:59,666 --> 00:04:00,300
Where is...

70
00:04:03,133 --> 00:04:06,866
Oh, there's Mario & Luigi: Dream
Team. Never mind. I was wondering where it was.

71
00:04:08,033 --> 00:04:10,933
I got confused. I thought I just saw the

72
00:04:10,933 --> 00:04:14,699
demo or the update
and then got very confused.

73
00:04:15,766 --> 00:04:16,266
Your Ratings.

74
00:04:19,033 --> 00:04:20,066
And we have...

75
00:04:21,833 --> 00:04:23,566
On the top screen it says "Your Ratings".

76
00:04:26,733 --> 00:04:28,933
And on the
bottom... It says here you can see

77
00:04:28,933 --> 00:04:30,966
the software you
have submitted ratings for.

78
00:04:32,133 --> 00:04:34,633
And on the bottom screen
it shows The Legend of Zelda: Four

79
00:04:34,633 --> 00:04:37,866
Swords Anniversary Edition
and Mario & Luigi: Dream Team.

80
00:04:38,233 --> 00:04:40,533
If you remember we
just did those a few... We just

81
00:04:40,533 --> 00:04:43,566
reviewed those a few
episodes ago. A few parts ago.

82
00:04:44,233 --> 00:04:46,266
And then there's a
button that says "Delete Ratings".

83
00:04:47,466 --> 00:04:47,966
And then...

84
00:04:49,333 --> 00:04:51,333
If you tap on one of them...

85
00:04:53,800 --> 00:04:58,166
It will show you how you rated
it. So Four Swords Anniversary Edition...

86
00:04:59,766 --> 00:05:04,666
Just has all the stuff there
and Dream Team has all the stuff there.

87
00:05:05,333 --> 00:05:08,300
What if I delete- click
"Delete Ratings"? It says this

88
00:05:08,300 --> 00:05:11,500
will delete all of the
user ratings you have submitted.

89
00:05:11,533 --> 00:05:14,233
This data cannot be
recovered once deleted. Are

90
00:05:14,233 --> 00:05:16,800
you sure you want to
delete your user ratings? No.

91
00:05:20,000 --> 00:05:20,699
Next one.

92
00:05:22,366 --> 00:05:23,399
Home Menu Notifications.

93
00:05:24,133 --> 00:05:27,100
The top screen says
"Home Menu Notifications".

94
00:05:28,033 --> 00:05:32,066
You can choose whether or not to
receive Nintendo eShop notifications, which may

95
00:05:32,066 --> 00:05:35,666
include commercial information
such as marketing or advertising messages.

96
00:05:36,466 --> 00:05:38,666
Such notifications
will be sent by Nintendo

97
00:05:38,666 --> 00:05:40,466
via SpotPass and
will appear on the Home Menu.

98
00:05:40,566 --> 00:05:43,733
That's when the eShop
is closed. Or the... That's

99
00:05:43,733 --> 00:05:47,266
when the 3DS is closed
if you have SpotPass turned on.

100
00:05:48,100 --> 00:05:51,566
Would you like to opt in
to receiving these notifications?

101
00:05:52,666 --> 00:05:55,066
"Receive
Notifications". Oh, that's... These are

102
00:05:55,066 --> 00:05:57,533
the notifications I
showed you in the top part there.

103
00:05:58,100 --> 00:06:00,466
You can either do
"Receive" or "Do Not Receive".

104
00:06:02,933 --> 00:06:04,533
You will now receive notifications from

105
00:06:04,533 --> 00:06:06,933
Nintendo eShop. You
can change this setting later.

106
00:06:07,966 --> 00:06:09,033
What if I turn it off?

107
00:06:10,800 --> 00:06:12,433
You will not receive notifications from

108
00:06:12,433 --> 00:06:15,033
Nintendo eShop. You
can change this setting later.

109
00:06:20,433 --> 00:06:22,166
Software Recommendations from Nintendo.

110
00:06:24,233 --> 00:06:30,133
On the top screen it says "Software
Recommendations from Nintendo". Please read the

111
00:06:30,133 --> 00:06:31,966
information on the
lower screen carefully.

112
00:06:33,133 --> 00:06:36,466
"Software Recommendations
from Nintendo & Information Sharing".

113
00:06:36,800 --> 00:06:38,733
You can opt in to sharing information

114
00:06:38,733 --> 00:06:41,366
about your Nintendo
eShop page viewing history,

115
00:06:42,233 --> 00:06:45,333
purchase history, and the titles
that you have added to your wishlist.

116
00:06:46,166 --> 00:06:48,600
If you opt in, the
information that you share with

117
00:06:48,600 --> 00:06:52,133
Nintendo may be used to
improve the Nintendo eShop service,

118
00:06:52,899 --> 00:06:56,666
make software recommendations to you,
and as reference when developing new products.

119
00:06:57,466 --> 00:06:59,366
Do you wish to share
this information with Nintendo?

120
00:07:01,100 --> 00:07:06,066
No or yes. I'm gonna say no. Your
usage information will not be sent to Nintendo.

121
00:07:09,399 --> 00:07:11,833
And then I assume
the other one is just it

122
00:07:11,833 --> 00:07:14,333
will be sent to
Nintendo. Language Settings.

123
00:07:15,300 --> 00:07:19,566
The top screen says, oh
and this one has like a pop-up

124
00:07:19,966 --> 00:07:20,433
dialogue

125
00:07:21,033 --> 00:07:23,333
rather than like an actual page. That's

126
00:07:23,333 --> 00:07:25,233
kind of weird. Here
you can set the language

127
00:07:25,666 --> 00:07:28,533
in which Nintendo eShop
is displayed. Select the language

128
00:07:28,533 --> 00:07:32,066
you want to use. The only
options are English and Spanish.

129
00:07:34,899 --> 00:07:38,933
Which is weird. I would imagine that
they would add in more than just Spanish.

130
00:07:40,000 --> 00:07:44,399
English and Spanish because
that's like, I mean sure those are the two

131
00:07:45,766 --> 00:07:50,133
most common languages
in the US, but I mean wouldn't

132
00:07:50,133 --> 00:07:53,300
you think that they
would put in more than just two?

133
00:07:54,199 --> 00:07:54,866
It's kind of weird.

134
00:07:55,899 --> 00:07:56,833
Location Settings.

135
00:07:58,766 --> 00:08:02,166
Oh, this is where you
can put in your address. Your

136
00:08:02,166 --> 00:08:05,233
address is set to the
following. I'm blocking all that out.

137
00:08:06,133 --> 00:08:09,033
This information on
the upper, the information on

138
00:08:09,033 --> 00:08:11,399
the upper screen is
used to calculate sales tax.

139
00:08:12,333 --> 00:08:14,166
To change your saved address, tap Change

140
00:08:14,166 --> 00:08:17,833
Address. So if we do
that, then it'll just go in,

141
00:08:18,866 --> 00:08:19,366
in there.

142
00:08:20,500 --> 00:08:23,266
What happens if I push delete?
Delete your address information? No.

143
00:08:25,466 --> 00:08:29,966
Um, About, About
Nintendo eShop, how much is there?

144
00:08:30,633 --> 00:08:34,533
Oh. About Nintendo eShop, please read the
information on the lower screen carefully.

145
00:08:34,566 --> 00:08:37,799
Nintendo eShop is a service
that allows you to download demos,

146
00:08:38,066 --> 00:08:42,466
purchase licenses for Nintendo and
third-party software, and find information about

147
00:08:42,466 --> 00:08:44,433
Nintendo and
third-party products and services.

148
00:08:45,200 --> 00:08:49,033
In addition to these benefits, you
can opt in to receive commercial updates on

149
00:08:49,033 --> 00:08:51,566
Nintendo and
third-party products and services

150
00:08:51,566 --> 00:08:55,033
via notifications on
your Nintendo 3DS Home Menu. If

151
00:08:55,033 --> 00:08:57,866
you want to stop
receiving these messages at any point,

152
00:08:57,866 --> 00:09:00,500
you can opt out by
tapping Settings/Other from the

153
00:09:00,500 --> 00:09:04,033
Nintendo eShop main menu and
selecting Home Menu Notifications.

154
00:09:05,233 --> 00:09:09,399
If you provide Nintendo with user
ratings about products and services provided by

155
00:09:09,399 --> 00:09:12,966
Nintendo or its third
parties, Nintendo may use such

156
00:09:13,366 --> 00:09:17,666
information on an
anonymous basis to create

157
00:09:17,666 --> 00:09:20,333
informational messages
and share information about

158
00:09:20,866 --> 00:09:25,533
products with consumers in the Title
Information and search functions of Nintendo

159
00:09:25,533 --> 00:09:28,533
eShop prior to the
purchase of a product license.

160
00:09:30,033 --> 00:09:33,666
The Nintendo 3DS Service User
Agreement applies when using this service.

161
00:09:34,033 --> 00:09:38,266
You can view the user agreement on
your Nintendo 3DS system or on our website at

162
00:09:38,433 --> 00:09:42,799
http colon slash slash www
dot nintendo dot com slash consumer.

163
00:09:44,766 --> 00:09:46,666
Verify Download Status is the next thing.

164
00:09:48,399 --> 00:09:53,233
Verification complete, download
status updated. I would have to go in here while

165
00:09:53,233 --> 00:09:56,100
I'm downloading something
to show you what it looks like.

166
00:09:57,066 --> 00:09:57,966
Customer Support.

167
00:10:00,833 --> 00:10:04,200
Oh. Oh! That's
interesting. It's a different scroll bar.

168
00:10:05,200 --> 00:10:05,399
Here.

169
00:10:05,799 --> 00:10:06,100
Whee!

170
00:10:06,433 --> 00:10:06,733
Sorry.

171
00:10:07,500 --> 00:10:10,133
Um, please read the
information on the lower screen.

172
00:10:10,866 --> 00:10:13,033
If anything is unclear
about Nintendo eShop, please

173
00:10:13,033 --> 00:10:15,633
visit the Nintendo
website before contacting Nintendo.

174
00:10:16,299 --> 00:10:19,933
It has the Nintendo homepage,
Nintendo customer support, general inquiries,

175
00:10:20,933 --> 00:10:25,333
where you can get online support,
self-service support, customer service,

176
00:10:26,233 --> 00:10:27,100
and what times,

177
00:10:27,966 --> 00:10:30,666
customer service available
in English, French, and Spanish.

178
00:10:31,533 --> 00:10:36,399
For inquiries regarding a specific
title, please refer to the list of publishers,

179
00:10:36,399 --> 00:10:39,466
publishers below, or to that
title's manual for contact information.

180
00:10:39,799 --> 00:10:41,733
Please note that
individual publishers will not

181
00:10:41,733 --> 00:10:44,433
answer questions
regarding game strategy or tips.

182
00:10:45,333 --> 00:10:48,833
And then this is just a
bunch of publishers and their domains.

183
00:10:50,200 --> 00:10:52,000
I'm just gonna kinda go down like this.

184
00:10:57,366 --> 00:11:00,566
The email addresses or websites.

185
00:11:07,533 --> 00:11:12,666
Oh, and then this one has a
phone number, website, and email.

186
00:11:14,466 --> 00:11:35,799
[Calm 3DS eShop music]

187
00:11:36,266 --> 00:11:39,433
It would be funny if one
of them had an option for fax.

188
00:11:40,066 --> 00:11:46,799
[Calm 3DS eShop music, continued]

189
00:11:47,799 --> 00:11:50,333
Oh, that's a lot that
Gameloft has, and it tells you

190
00:11:50,333 --> 00:11:53,566
when when their customer
support is available for Gameloft.

191
00:11:54,433 --> 00:12:02,333
[Calm 3DS eShop music, continued, continued]

192
00:12:03,166 --> 00:12:08,266
Oh, Hudson Soft. I don't
think support's available anymore.

193
00:12:10,966 --> 00:12:11,566
(surprised) Next!

194
00:12:14,433 --> 00:12:33,866
[Calm 3DS eShop music, continued, continued,
continued]

195
00:12:33,866 --> 00:12:34,533
Netflix.

196
00:12:37,833 --> 00:12:41,866
Oh, yeah, I remember Netflix
on the Wii, apparently, was working...

197
00:12:43,700 --> 00:12:46,866
at some point
recently. When I say recently, I

198
00:12:46,866 --> 00:12:49,600
mean like within the
last six months or something.

199
00:12:50,533 --> 00:12:52,766
Oh, ReMedia has no contact information.

200
00:12:55,566 --> 00:12:59,399
Oh, yeah, I remember it was
actually working for some reason, which is...

201
00:13:00,133 --> 00:13:00,966
which is weird.

202
00:13:04,333 --> 00:13:08,566
And then it was reporting Netflix
for Wii. I think it's the disc version.

203
00:13:09,566 --> 00:13:11,966
Not the Wii Shop Channel version.

204
00:13:14,066 --> 00:13:19,233
Said that it said that
it didn't support the iPad.

205
00:13:20,033 --> 00:13:23,066
Like Netflix for iPad, some old version.

206
00:13:25,200 --> 00:13:26,733
Which is weird because it's not an iPad.

207
00:13:29,933 --> 00:13:34,533
And then... oh, we're
at the end, almost. And then...

208
00:13:37,100 --> 00:13:37,500
There we go.

209
00:13:40,233 --> 00:13:40,366
Oh.

210
00:13:49,066 --> 00:13:50,666
When you tap this, it makes a noise.

211
00:13:56,833 --> 00:14:01,266
And you can just go up and down
like that. You can just touch somewhere,

212
00:14:02,166 --> 00:14:04,433
tap somewhere on the
scroll bar, and it will scroll there.

213
00:14:05,866 --> 00:14:10,366
Apparently someone at Netflix
changed it so that it wouldn't even try to

214
00:14:11,166 --> 00:14:13,766
do that anymore, and
then couldn't use it anymore.

215
00:14:14,433 --> 00:14:15,733
Customer Support- Oh, that's it?

216
00:14:16,533 --> 00:14:16,866
Alright.

217
00:14:20,100 --> 00:14:21,766
Next on the list is...

218
00:14:23,266 --> 00:14:27,266
way over here on the left. Settings/Other, we were literally just in there.

219
00:14:27,833 --> 00:14:30,566
Oh, yeah. This is...
yeah, that's all it meant.

220
00:14:31,366 --> 00:14:31,933
Add funds.

221
00:14:34,233 --> 00:14:37,533
I will go through this
just to make sure it's the same whoops.

222
00:14:40,633 --> 00:14:42,799
Just to make sure
it's the same, it should be.

223
00:14:43,700 --> 00:14:44,266
Add funds.

224
00:14:46,466 --> 00:14:49,033
Add funds is the same. Rate titles.

225
00:14:55,299 --> 00:14:58,833
Yeah, this is the same as the...

226
00:15:00,533 --> 00:15:02,433
the other menu. Wish list.

227
00:15:03,333 --> 00:15:06,933
[A different calm track from the 3DS eShop]

228
00:15:08,500 --> 00:15:08,799
Search.

229
00:15:14,166 --> 00:15:17,466
Oh, with this
search, you can only use filters.

230
00:15:21,633 --> 00:15:23,933
Charts. Ah, now this is different.

231
00:15:25,966 --> 00:15:27,833
Uh, on the top screen it says Charts.

232
00:15:28,733 --> 00:15:30,566
View software and video charts.

233
00:15:31,399 --> 00:15:35,933
Select the chart that you want to
view. Software charts: recent releases. Your

234
00:15:35,933 --> 00:15:38,066
choices are recent
releases, all software,

235
00:15:38,833 --> 00:15:40,700
recent ratings, all user ratings,

236
00:15:41,833 --> 00:15:46,066
and video charts: recently popular.
So we're going to go to recent releases.

237
00:15:50,100 --> 00:15:53,633
Um, okay. Pokemon
Crystal Version, English Version.

238
00:15:54,266 --> 00:15:57,066
Five stars. Oh, at the
top it says software charts.

239
00:15:58,433 --> 00:16:01,433
Currently viewing:
recent releases, all software.

240
00:16:03,633 --> 00:16:06,333
Number one is Pokemon Crystal
version, and there's like a ribbon.

241
00:16:07,500 --> 00:16:12,733
Gold... gold thing with a ribbon,
blue ribbon behind it on the top left.

242
00:16:14,033 --> 00:16:16,133
And this is just a
vertically scrolling list.

243
00:16:16,833 --> 00:16:17,899
Pokemon Crystal, Yellow...

244
00:16:18,966 --> 00:16:23,299
Oh, and when you touch one of
them... Oh, that's entirely a button.

245
00:16:24,266 --> 00:16:27,066
So it's like it turns orange. It's gray.

246
00:16:27,933 --> 00:16:30,533
When you press down
on it and it turns orange.

247
00:16:31,299 --> 00:16:34,333
That looks very nice and satisfying.

248
00:16:35,899 --> 00:16:37,966
It looks very nice
and visually satisfying.

249
00:16:43,399 --> 00:16:45,299
Yes, I like that very much. Filter by.

250
00:16:46,366 --> 00:16:49,633
We've got Select the platform
you want to view charts for: all software,

251
00:16:50,433 --> 00:16:55,100
Nintendo 3DS download, Nintendo
DSiWare, Virtual Console all. Let's do DSiWare.

252
00:16:58,166 --> 00:16:59,966
And we've got Photo Dojo.

253
00:17:01,233 --> 00:17:02,700
Number one with four stars.

254
00:17:03,633 --> 00:17:05,666
Mario vs. Donkey
Kong: Minis March Again.

255
00:17:06,433 --> 00:17:09,633
With four and a half stars at
number two. I don't know how that works.

256
00:17:10,433 --> 00:17:12,200
And WarioWare: Snapped at four stars.

257
00:17:12,966 --> 00:17:13,233
Y'know.

258
00:17:16,299 --> 00:17:19,333
Let's go back into
charts. Uh, recent ratings.

259
00:17:19,666 --> 00:17:23,066
[3DS eShop music]

260
00:17:23,266 --> 00:17:25,366
So we've got Apollo Justice: Ace Attorney,

261
00:17:26,866 --> 00:17:29,700
Kirby Planet Robobot, Monster Hunter 4

262
00:17:29,700 --> 00:17:33,066
Ultimate. You know,
recent ratings, all software.

263
00:17:34,766 --> 00:17:36,966
You know, that's very nice.

264
00:17:37,766 --> 00:17:39,533
But you have to go back into charts.

265
00:17:40,799 --> 00:17:44,566
Um, all user ratings. Let's do that.

266
00:17:44,599 --> 00:17:48,466
[music]

267
00:17:48,733 --> 00:17:53,733
Um, Japanese Rail
Sim 3D Travel of Steam,

268
00:17:54,466 --> 00:17:57,266
Legend of Zelda Ballad
of The Goddess, Yo-kai Watch 3,

269
00:17:58,200 --> 00:18:00,366
Legend of Zelda
Great Fairy's Fountain Theme.

270
00:18:01,099 --> 00:18:01,900
That's interesting.

271
00:18:02,866 --> 00:18:04,599
That's the, I guess that's the Wind Waker

272
00:18:04,599 --> 00:18:06,433
version because
that's a Wind Waker Great Fairy.

273
00:18:07,266 --> 00:18:10,666
Make sure to check out
my unfinished Wind Waker series.

274
00:18:11,533 --> 00:18:13,733
I'm kidding. You don't have
to check it out if you don't want to.

275
00:18:15,900 --> 00:18:18,866
Uh, video charts, recently
popular. Let's see how this shows up.

276
00:18:21,900 --> 00:18:26,233
Pokémon Bank Transfer, a
minute and 34 seconds, recently popular.

277
00:18:26,766 --> 00:18:29,933
Pokémon Bank
Transfer, Kirby Planet Robobot,

278
00:18:30,400 --> 00:18:34,733
Overview Trailer, 4
minutes 56 seconds, Tomodachi

279
00:18:35,466 --> 00:18:38,733
Life Wish List
Video, 2 minutes 18 seconds.

280
00:18:39,233 --> 00:18:41,966
Pokémon Yellow
Version Trailer, 29 seconds.

281
00:18:43,166 --> 00:18:46,166
Yeah, that's ESRB E.

282
00:18:47,333 --> 00:18:51,933
So some things do show up
as the rating, but others don't.

283
00:18:53,033 --> 00:18:56,333
So let's go into Pokémon Bank
Transfer and see how that shows up.

284
00:18:57,200 --> 00:18:58,933
Okay, it just has the video here.

285
00:18:59,666 --> 00:19:01,133
We can go to software information.

286
00:19:02,266 --> 00:19:05,633
And then this takes us to Pokémon Bank.

287
00:19:08,966 --> 00:19:12,333
Oh, okay. This is,
okay, I guess I should read this.

288
00:19:12,633 --> 00:19:15,299
Pokémon Bank. Offers
in-game purchases. Nintendo

289
00:19:15,299 --> 00:19:18,900
3DS Download Only.
Release date: February 5th, 2014.

290
00:19:20,066 --> 00:19:23,900
In late March, 2023, paid passes
will no longer be offered for purchase,

291
00:19:24,533 --> 00:19:28,466
and the Pokémon Bank service
will become available at no cost to users.

292
00:19:29,266 --> 00:19:32,266
For more information,
please visit Pokemon.com/bank.

293
00:19:34,133 --> 00:19:36,666
Pokémon Bank is an application paid

294
00:19:36,666 --> 00:19:41,733
service with an annual
fee of US $5, Canada $7.09

295
00:19:42,833 --> 00:19:45,733
that will allow you to
deposit, store and manage up to

296
00:19:45,733 --> 00:19:48,366
3,000 of your Pokémon in
private boxes on the internet.

297
00:19:49,433 --> 00:19:54,666
It will be, I don't know if it is, but I

298
00:19:54,666 --> 00:19:56,500
assume that this was
written before it was released.

299
00:19:57,233 --> 00:19:58,066
It will... It will...

300
00:20:01,266 --> 00:20:03,733
It will be a
powerful resource for players who

301
00:20:03,733 --> 00:20:06,400
like to obtain many
different kinds of Pokémon.

302
00:20:06,933 --> 00:20:10,833
Or for those who
like to raise many Pokémon

303
00:20:10,833 --> 00:20:13,033
in preparation for
battles and competitions.

304
00:20:14,099 --> 00:20:19,299
Pokémon Bank also makes it easy to
transfer Pokémon from copies of Pokémon X, Y,

305
00:20:20,000 --> 00:20:25,033
Omega Ruby, Alpha
Sapphire, Sun, Moon, Red, Blue, Yellow,

306
00:20:27,966 --> 00:20:32,033
Ultra Sun, and Ultra Moon, including
the downloadable versions to a single game.

307
00:20:32,533 --> 00:20:36,433
When you download Pokémon Bank, you
can also download the free link application,

308
00:20:36,633 --> 00:20:37,466
Poké Transporter.

309
00:20:38,266 --> 00:20:38,900
Here's screenshots.

310
00:20:40,166 --> 00:20:44,866
Here's group 1, 100, box 1,
and a bunch of different Pokémon.

311
00:20:45,866 --> 00:20:46,966
And on the bottom screen...

312
00:20:48,433 --> 00:20:49,633
Wait, box...

313
00:20:51,266 --> 00:20:54,099
I'm a little confused
about what's going on here.

314
00:20:54,133 --> 00:20:59,000
I've never used
this application, and I'm not

315
00:20:59,000 --> 00:21:02,366
sure why there's box
1 on top and box 1 on the

316
00:21:02,366 --> 00:21:06,166
bottom, but with
different... with different Pokémon.

317
00:21:07,733 --> 00:21:11,366
So, but the cursor's on
Pikachu on the bottom screen.

318
00:21:13,066 --> 00:21:16,866
And yeah, there's a
list button and a save button.

319
00:21:18,000 --> 00:21:18,833
And it's apparently...

320
00:21:18,866 --> 00:21:21,966
[music]

321
00:21:22,599 --> 00:21:25,133
And this is
apparently Pokémon X or something.

322
00:21:25,833 --> 00:21:30,000
And the next screenshot shows a chart of

323
00:21:30,000 --> 00:21:33,166
various Pokémon and
their type, nature, and marking.

324
00:21:34,566 --> 00:21:35,233
And you can display...

325
00:21:35,533 --> 00:21:38,333
On the bottom screen, it
says you can display Pokémon by name.

326
00:21:39,066 --> 00:21:40,433
Remove search condition: electric.

327
00:21:40,933 --> 00:21:42,133
Oh, this is search results.

328
00:21:43,033 --> 00:21:45,233
And display all and go to Pikachu's box.

329
00:21:45,666 --> 00:21:49,066
Go to the box with this
specific highlighted Pokémon.

330
00:21:53,466 --> 00:21:54,066
And that's it.

331
00:21:55,099 --> 00:21:55,900
Only two screenshots.

332
00:21:59,000 --> 00:22:01,133
Pokémon Bank,
Pokémon Company, one player,

333
00:22:01,666 --> 00:22:04,933
genre: application,
languages: English, Spanish,

334
00:22:04,933 --> 00:22:05,466
and French.

335
00:22:05,866 --> 00:22:10,933
I really like the
theme in this application.

336
00:22:12,133 --> 00:22:13,000
Looks very nice.

337
00:22:15,666 --> 00:22:17,233
You may need an SD card.

338
00:22:18,366 --> 00:22:19,233
All these...

339
00:22:19,666 --> 00:22:22,466
All the Pokémon
games on... available on the

340
00:22:22,466 --> 00:22:25,833
3DS are the only games
from which you may transfer

341
00:22:25,833 --> 00:22:27,633
Pokémon directly into the Pokémon Bank.

342
00:22:28,333 --> 00:22:30,866
At least one of these games
is required to use Pokémon Bank.

343
00:22:31,299 --> 00:22:33,366
In-game content
available for purchase using.

344
00:22:33,833 --> 00:22:37,533
In-game content available for
purchase using stored account funds.

345
00:22:38,766 --> 00:22:44,099
Oh, it's available for purchase
using the stored funds in the eShop account.

346
00:22:44,466 --> 00:22:45,066
I get it.

347
00:22:46,500 --> 00:22:47,966
Compatible features slash accessories.

348
00:22:48,433 --> 00:22:50,433
Nintendo Network ID
and supports online play.

349
00:22:50,866 --> 00:22:52,000
Websites and Nintendo website.

350
00:22:52,599 --> 00:22:53,866
And Pokémon X and Y website.

351
00:22:54,933 --> 00:22:57,333
Oh, that's the first
one I've seen with two websites.

352
00:22:58,433 --> 00:22:59,000
There's ratings.

353
00:23:00,333 --> 00:23:03,533
For everyone, it's 50, 61%.

354
00:23:04,733 --> 00:23:05,533
What type of game is it?

355
00:23:05,533 --> 00:23:06,933
Casual play, 79%.

356
00:23:07,866 --> 00:23:08,966
Related genres: Other.

357
00:23:10,766 --> 00:23:13,766
Related
keywords: application and Pokémon.

358
00:23:14,266 --> 00:23:16,000
Copyright 2013 Pokémon.

359
00:23:16,766 --> 00:23:20,200
95 through 2013
Nintendo/Creatures Inc.

360
00:23:21,133 --> 00:23:22,166
/Game Freak, Inc.

361
00:23:22,833 --> 00:23:25,433
Pokémon and Nintendo
3DS are trademarks of Nintendo.

362
00:23:29,066 --> 00:23:29,333
Nope.

363
00:23:32,133 --> 00:23:33,000
And there's that.

364
00:23:36,566 --> 00:23:38,233
Next one, recent arrivals.

365
00:23:39,500 --> 00:23:40,033
Oh, okay.

366
00:23:40,966 --> 00:23:43,400
On the top screen,
it says recent arrivals.

367
00:23:43,833 --> 00:23:45,466
Check out the latest software and videos.

368
00:23:46,133 --> 00:23:48,400
The bottom screen says
new software and latest videos.

369
00:23:48,400 --> 00:23:49,266
Let's go new software.

370
00:23:53,633 --> 00:23:56,766
Fragrant Story Update
version 1.1, software update.

371
00:23:57,266 --> 00:23:58,466
Available now in Nintendo eShop.

372
00:23:59,299 --> 00:24:01,533
Mario Kart 7 update version 1.2.

373
00:24:01,533 --> 00:24:02,400
Oh yeah, I remember this.

374
00:24:03,566 --> 00:24:04,033
This one.

375
00:24:06,766 --> 00:24:09,500
I don't remember exactly what it did.

376
00:24:11,866 --> 00:24:14,099
Mario Kart 7 update version 1.2.

377
00:24:14,533 --> 00:24:16,566
December 13th, 2022.

378
00:24:17,400 --> 00:24:19,333
This is an update
for the Mario Kart 7 game.

379
00:24:19,533 --> 00:24:21,866
This download does not
contain the full version of the title.

380
00:24:22,566 --> 00:24:24,366
In order to use this
update, you must have the full

381
00:24:24,366 --> 00:24:26,333
version of Mario Kart
7, which is sold separately.

382
00:24:26,866 --> 00:24:29,833
Downloading this update will
resolve shortcut exploits in the following

383
00:24:29,833 --> 00:24:31,400
online multiplayer mode courses:

384
00:24:32,166 --> 00:24:34,633
Wuhu Loop, Maka
Wuhu, and Bowser Castle 1.

385
00:24:36,966 --> 00:24:41,566
Starting May 16th, 2012, this update
will be required for online multiplayer mode.

386
00:24:41,900 --> 00:24:43,666
You can still play single player or local

387
00:24:43,666 --> 00:24:45,266
multiplayer
without installing the update.

388
00:24:46,166 --> 00:24:48,400
Once the update is
complete, the update will be installed.

389
00:24:48,733 --> 00:24:49,666
The download is complete.

390
00:24:49,933 --> 00:24:51,366
The update will be
installed automatically.

391
00:24:51,900 --> 00:24:54,333
The update will be visible under system

392
00:24:54,333 --> 00:24:58,166
settings, data
management, Nintendo 3DS, add-on content.

393
00:24:58,533 --> 00:25:00,900
There will be no gift icon displayed on

394
00:25:00,900 --> 00:25:03,166
the home menu after
the download is complete.

395
00:25:03,833 --> 00:25:06,366
When Mario Kart 7
is launched, version 1.2

396
00:25:06,366 --> 00:25:08,466
will appear above
the online multiplayer icon

397
00:25:08,933 --> 00:25:10,033
if the update was successful.

398
00:25:10,566 --> 00:25:13,400
When Mario Kart 7
is launched, version 1.2

399
00:25:13,400 --> 00:25:15,533
will appear above
the online multiplayer icon

400
00:25:16,133 --> 00:25:17,466
if the update was successful.

401
00:25:17,733 --> 00:25:19,500
Deleting the update will require you to

402
00:25:19,500 --> 00:25:21,333
download it again
to play multiplayer online.

403
00:25:22,533 --> 00:25:25,900
Mario Kart 7 update, version
1.2, publisher: Nintendo, genre: updates,

404
00:25:26,700 --> 00:25:27,366
languages: English.

405
00:25:28,466 --> 00:25:33,366
It's an important
update for Mario Kart 7.

406
00:25:33,833 --> 00:25:39,266
This is an important update for
Mario Kart 7 to resolve shortcut exploits

407
00:25:39,766 --> 00:25:41,066
in online multiplayer modes.

408
00:25:41,433 --> 00:25:44,066
I think that this stuff was up there too.

409
00:25:44,433 --> 00:25:46,299
Everyone rated E, comic mischief and all that.

410
00:25:47,333 --> 00:25:48,166
Ratings: three.

411
00:25:48,166 --> 00:25:49,533
People actually rated this.

412
00:25:50,566 --> 00:25:51,133
Huh, interesting.

413
00:25:51,533 --> 00:25:54,533
Three, only three players rated it.

414
00:25:55,033 --> 00:25:56,733
What type of person is it suited for?

415
00:25:56,733 --> 00:25:58,266
Everyone 67%.

416
00:25:58,700 --> 00:25:59,766
What type of game is it?

417
00:26:00,066 --> 00:26:01,633
67% casual play.

418
00:26:02,966 --> 00:26:04,166
Related genres: updates.

419
00:26:06,233 --> 00:26:11,166
And then the actual most
recent game is CosmiBall 3D.

420
00:26:11,933 --> 00:26:15,566
Guardian- and then
Guardians and Metal Exterminators.

421
00:26:17,566 --> 00:26:19,666
And then Hunting and
Camping in a Singularity.

422
00:26:21,666 --> 00:26:22,433
Sounds disturbing.

423
00:26:23,766 --> 00:26:25,033
Silver Falls Ghoul Busters.

424
00:26:27,866 --> 00:26:30,433
That's actually pretty
creative, calling it Ghoul Busters

425
00:26:31,233 --> 00:26:32,533
instead of Ghostbusters.

426
00:26:34,500 --> 00:26:36,200
Automaton Lung.

427
00:26:38,966 --> 00:26:39,733
(surprised) One star!

428
00:26:42,066 --> 00:26:43,933
An actual one star game.

429
00:26:43,933 --> 00:26:45,166
Let's see how bad this is.

430
00:27:10,599 --> 00:27:10,799
Okay.

431
00:27:17,900 --> 00:27:20,466
Oh, only on Ninte- New Nintendo 3DS.

432
00:27:21,733 --> 00:27:22,599
("One" said like "Wha?!")

433
00:27:23,533 --> 00:27:25,233
One star rating.

434
00:27:25,733 --> 00:27:26,333
It's for everyone.

435
00:27:27,033 --> 00:27:29,799
And it's for intense pl- (barely holding it together) intense play.

436
00:27:32,700 --> 00:27:33,400
Okay, okay.

437
00:27:37,700 --> 00:27:40,266
And now, we can go to the latest videos.

438
00:27:43,200 --> 00:27:44,233
I like this music.

439
00:27:46,466 --> 00:27:47,833
Hotel Transylvania video.

440
00:27:48,733 --> 00:27:51,566
I didn't know that there
was a game for Hotel Transylvania.

441
00:27:53,333 --> 00:27:55,799
Uh, Slime Slayer
trailer with song lyrics.

442
00:27:56,833 --> 00:27:58,599
Without Escape trailer, Let's chop.

443
00:27:59,833 --> 00:28:03,166
Persona Q2: New Cinema Labyrinth trailer.

444
00:28:03,799 --> 00:28:05,099
Ok, now back.

445
00:28:05,966 --> 00:28:06,733
Recent Arrivals.

446
00:28:07,166 --> 00:28:07,366
News.

447
00:28:07,933 --> 00:28:09,366
There are currently no news items.

448
00:28:10,066 --> 00:28:12,400
If you wait long
enough, there will be text up here.

449
00:28:12,766 --> 00:28:13,566
That will show up.

450
00:28:13,566 --> 00:28:14,133
Here we go.

451
00:28:14,733 --> 00:28:22,366
It is no longer possible to add
funds directly to an account using this device.

452
00:28:24,200 --> 00:28:32,599
You can still add funds off device
using a connected Nintendo Account wallet

453
00:28:32,933 --> 00:28:35,933
until March 27, 2023.

454
00:28:36,866 --> 00:28:40,566
To learn more, visit support.nintendo.com

455
00:28:40,599 --> 00:28:42,566
slash 3DS slash eShop.

456
00:28:43,200 --> 00:28:44,233
That's all that there is there.

457
00:28:44,233 --> 00:28:45,400
And then there's games.

458
00:28:45,733 --> 00:28:48,566
But that's all
we're gonna do for this part.

459
00:28:49,299 --> 00:28:53,033
So in the next part, we're
gonna go through this list here.

460
00:28:54,933 --> 00:28:55,933
And then this one.

461
00:28:59,233 --> 00:29:01,733
So, goodbye for now.

