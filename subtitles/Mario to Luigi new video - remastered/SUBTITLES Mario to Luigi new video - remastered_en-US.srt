1
00:00:09,019 --> 00:00:14,579
Okay, so, today I will be showing you how

2
00:00:14,579 --> 00:00:22,139
to turn Mario into
Luigi, sort of, in Super

3
00:00:22,199 --> 00:00:29,360
Mario 64. This
doesn't actu-, it doesn't look

4
00:00:29,360 --> 00:00:34,179
exactly like Luigi
and the sounds are still

5
00:00:34,179 --> 00:00:44,139
Mario sounds. So this is as good as a lot

6
00:00:44,139 --> 00:00:47,260
of people can get
to, or this is as good as

7
00:00:47,300 --> 00:00:50,340
I can get to. I don't know how to change

8
00:00:50,340 --> 00:00:55,019
the heads up
display yet, but anyway I will

9
00:00:55,800 --> 00:01:01,159
do this now. So
first of all you will need

10
00:01:01,159 --> 00:01:08,320
to extend your
ROM with the Mario 64 ROM

11
00:01:08,460 --> 00:01:16,900
Extender and if
you put it into Toad's Tool

12
00:01:16,900 --> 00:01:21,119
it won't accept it
otherwise you have to extend

13
00:01:21,119 --> 00:01:24,539
it, so let's go through and do that now.

14
00:01:25,280 --> 00:01:28,280
And you can just ignore this.

15
00:01:28,820 --> 00:01:30,719
And you hit "Open ROM..."

16
00:01:33,199 --> 00:01:34,920
(thinking)

17
00:01:39,420 --> 00:01:40,260
Desktop

18
00:01:47,280 --> 00:01:49,579
My cat's name is Mitty.

19
00:01:52,059 --> 00:02:04,320
(SM64 Stuff), Tools, and Super
Mario 64 U and the parentheses.

20
00:02:05,260 --> 00:02:13,500
dot Z64. So click
on that and hit open. And it'll

21
00:02:13,539 --> 00:02:17,199
go through and do
(for) "In:". It'll decompress and

22
00:02:17,199 --> 00:02:22,820
move the MIO 0 data.
Send that in and then send out

23
00:02:22,820 --> 00:02:27,420
this, only that one
is extended. Now I'm using

24
00:02:27,420 --> 00:02:32,599
Windows 7 and so
this will vary depending on

25
00:02:32,599 --> 00:02:39,119
what operating
system and how old your computer

26
00:02:39,119 --> 00:02:41,579
is and how much RAM
it has. Now it's remapping

27
00:02:41,739 --> 00:02:46,199
pointers. Now it's
done. So there's not usually

28
00:02:46,199 --> 00:02:53,739
anything in the
debugger. So you just hit close

29
00:02:53,739 --> 00:02:57,360
and now there's a
freshly extended one right

30
00:02:57,360 --> 00:03:04,019
there. And so I will
pull it up and I will turn

31
00:03:04,019 --> 00:03:05,480
off the cheat codes.

32
00:03:12,179 --> 00:03:14,300
And I will reset it...

33
00:03:20,519 --> 00:03:24,940
to show you that it is actually Mario.

34
00:03:26,639 --> 00:03:28,340
See it's Mario?

35
00:03:31,980 --> 00:03:36,739
(Mario yahooing all over the place)

36
00:03:43,280 --> 00:03:47,019
And anyway so yeah
that's normal Mario right

37
00:03:47,019 --> 00:03:50,519
there. And then
the only difference between

38
00:03:50,519 --> 00:03:55,679
this one and this
one other than the fact that

39
00:03:55,679 --> 00:04:01,300
the name, the file
name is different. This one

40
00:04:01,559 --> 00:04:05,980
is down here you
can see 24 megabytes while

41
00:04:05,980 --> 00:04:13,019
this one is only 8.
And then so now we will take

42
00:04:13,039 --> 00:04:23,960
Toad's Tool 64
version 0.5994b and open it and

43
00:04:23,960 --> 00:04:31,500
double click in
here. It will ask for a file.

44
00:04:33,360 --> 00:04:36,360
So we go to Desktop

45
00:04:38,500 --> 00:04:44,139
And then, we go to...

46
00:04:48,960 --> 00:04:51,159
here, and Tools...

47
00:04:53,760 --> 00:04:56,760
We open the extended one...

48
00:04:57,500 --> 00:05:00,500
Not the (regular) one, the extended
ROM.

49
00:05:01,599 --> 00:05:06,559
And it'll do all the stuff it
does. It might take a while the first time that

50
00:05:06,559 --> 00:05:11,579
you, every time that
you load a ROM the first time.

51
00:05:15,840 --> 00:05:19,800
Yeah and then it draws the
level. That usually takes only a

52
00:05:19,800 --> 00:05:26,300
few seconds. Okay so the
next part as you can see this is the

53
00:05:26,820 --> 00:05:37,820
castle grounds. So I want to go to
edit textures. Now we are in here and M is

54
00:05:37,820 --> 00:05:44,699
Mario's cap. So now we
have to find the L Is Real 2401...

55
00:05:46,000 --> 00:05:47,820
(trying to find it)

56
00:05:48,559 --> 00:05:52,880
thing, the star that is behind the
castle grounds.

57
00:05:54,960 --> 00:05:57,960
I'm still trying to find, ah there

58
00:05:57,960 --> 00:06:06,559
it is. L Is Real 2401. I don't
remember anything about that
(the bottom text). So then we go down

59
00:06:06,559 --> 00:06:12,699
here and we highlight this
under Mario's size. We change it to

60
00:06:13,599 --> 00:06:31,360
L Is Real and we hit Enter. Now, the
colors are Luigi and the size is 1.5. So I

61
00:06:31,360 --> 00:06:35,079
think we have to save colors to
the ROM, I can't remember if we have to.

62
00:06:37,360 --> 00:06:40,360
And then we just uh, close this.

63
00:06:41,920 --> 00:06:43,139
Now it's saved.

64
00:06:43,139 --> 00:06:46,139
And then we open this.

65
00:06:50,239 --> 00:06:51,980
(Mario introduces himself)

66
00:06:54,860 --> 00:06:57,159
(Mario says hi)

67
00:06:57,659 --> 00:06:59,059
(Mario says "Okie-dokie!")

68
00:06:59,860 --> 00:07:01,579
Now it's Luigi colors!

69
00:07:02,840 --> 00:07:06,539
Uh, please note that um...

70
00:07:08,079 --> 00:07:11,219
The sounds are not Luigi sounds.

71
00:07:11,719 --> 00:07:18,980
(Luigi wah-hoohooing all over the place
but sounding like Mario)

72
00:07:18,980 --> 00:07:21,980
(Luigi woahing when grabbing
the tree like Mario)

73
00:07:22,320 --> 00:07:23,800
They are still Mario.

74
00:07:25,659 --> 00:07:28,659
(Luigi yahooing all over the place
like Mario)

75
00:07:29,059 --> 00:07:41,639
And another thing is
notice how far apart um... his arms

76
00:07:41,639 --> 00:07:45,739
are from his head?
With Mario that's not the case.

77
00:07:48,079 --> 00:07:54,420
Now let's go um inside the castle.
Actually I will pause CamStudio for a second.

78
00:07:55,900 --> 00:08:01,079
And I will um get the... I will go
to the secret slide and get

79
00:08:01,079 --> 00:08:05,559
that star and show
you another thing that I mean.

80
00:08:07,019 --> 00:08:13,159
So now I'm playing the Secret Slide
and I wanna ground-pound this.

81
00:08:13,159 --> 00:08:15,039
And um...

82
00:08:16,560 --> 00:08:18,039
Actually if you

83
00:08:18,100 --> 00:08:23,939
long jump like what I tried to do. I
didn't do it right but if you long jump, you

84
00:08:23,939 --> 00:08:28,239
can actually make it
down to the end of the slide

85
00:08:28,239 --> 00:08:32,699
within the time limit
to get another extra star.

86
00:08:33,720 --> 00:08:41,340
And then another thing is the icon
for health. And then there is a life up here.

87
00:08:42,180 --> 00:08:48,979
The icon for the health, I mean the lives up
here is still Mario. It's not Luigi.

88
00:08:50,720 --> 00:08:52,979
And um, so...

89
00:08:54,199 --> 00:08:57,199
(broken box boing and twinkling Star sounds)

90
00:09:00,720 --> 00:09:02,260
(Luigi celebrating the Star he grabbed
by saying "Here we go!")

91
00:09:02,260 --> 00:09:05,260
So that looks like Luigi's kinda tall,

92
00:09:05,260 --> 00:09:05,960
now watch this...

93
00:09:05,960 --> 00:09:08,960
That's just Mario!

94
00:09:08,960 --> 00:09:17,439
And that's just Mario squished up.
There is quite a big difference there like that.

95
00:09:19,760 --> 00:09:22,760
Ok, goodbye.

