1
00:00:00,300 --> 00:00:05,200
Hello everybody,
today I am going to show you

2
00:00:05,200 --> 00:00:09,300
an updated version
of my Dolphin Emulator Mode.

3
00:00:09,933 --> 00:00:14,300
Or, "Close stuff that does not
need to run when Dolphin is open.bat".

4
00:00:15,666 --> 00:00:21,000
As you can see I have gotten
rid of all my icons on the desktop.

5
00:00:22,066 --> 00:00:24,966
And you can see right here it's disabled.

6
00:00:25,966 --> 00:00:28,466
Don't have me enable it. It's awful.

7
00:00:28,466 --> 00:00:31,866
Like, icons here, here, here.

8
00:00:32,566 --> 00:00:34,200
All the way over to like here.

9
00:00:34,866 --> 00:00:37,633
And here and here and like that.

10
00:00:37,633 --> 00:00:40,666
And then a few icons in
the center. It's just awful.

11
00:00:41,533 --> 00:00:43,500
Anyway I read something online that says

12
00:00:43,500 --> 00:00:46,166
that if you are
going to record your desktop

13
00:00:46,500 --> 00:00:51,133
you should make sure that you
don't have any icons on it generally.

14
00:00:51,866 --> 00:00:55,133
Because otherwise people
will think "yuck, that's gross."

15
00:00:55,500 --> 00:01:01,366
Or whatever. I'm running
Open Broadcast Software by the way.

16
00:01:03,200 --> 00:01:06,166
Anyway here is my new version of it. Yay!

17
00:01:07,033 --> 00:01:07,966
Aht- whoop...

18
00:01:08,233 --> 00:01:09,466
an-uhmm

19
00:01:09,700 --> 00:01:11,000
Close stuff that...

20
00:01:11,333 --> 00:01:16,599
(singing it like the SMB1 Overworld theme)
Close stuff that does not need
to run when playing on Dolphin.bat.

21
00:01:19,599 --> 00:01:20,933
Note-pad.

22
00:01:22,066 --> 00:01:23,700
If you
could tell what I was trying

23
00:01:23,700 --> 00:01:26,033
to get at there
then post it in the comments.

24
00:01:31,066 --> 00:01:33,366
I actually wasn't
trying to do that but anyway.

25
00:01:34,633 --> 00:01:38,633
As the last version had these.

26
00:01:39,000 --> 00:01:43,466
Classic Start Menu, Google
Drive Sync, iTunes Helper, iTunes EXE,

27
00:01:44,500 --> 00:01:51,566
Network Indicator. Network Indicator... that's not
something that's installed on Windows by default.

28
00:01:51,566 --> 00:01:55,233
It's something called...

29
00:02:02,400 --> 00:02:04,533
Unless it's in my startup folder?

30
00:02:05,700 --> 00:02:09,833
I know I need to
actually work on the video but

31
00:02:10,733 --> 00:02:13,666
I'm trying to show you what it is.

32
00:02:14,333 --> 00:02:18,033
No I don't have it.
Basically what it is is it makes it...

33
00:02:18,866 --> 00:02:19,733
It's like a um...

34
00:02:21,500 --> 00:02:29,066
It brings the Windows XP
two-monitors icon back, er, forward to Windows 7.

35
00:02:30,000 --> 00:02:32,500
Windows 7 all they have is a little...

36
00:02:36,000 --> 00:02:40,533
mobile phone tower
bars thing for the Wi-Fi.

37
00:02:41,166 --> 00:02:43,933
But I, I like the interface of the old one.

38
00:02:45,000 --> 00:02:50,666
So I may or might not
put a picture on the screen.

39
00:02:51,699 --> 00:02:55,833
I, I hope I do but if I don't then...
eh, oh well...

40
00:02:56,500 --> 00:02:58,666
Maybe I'll actually do
a video on that sometime.

41
00:02:59,933 --> 00:03:04,233
SDTray, Spybot Search & Destroy.
That takes a lot of RAM.

42
00:03:05,666 --> 00:03:09,166
You can, you can remove any
of these lines if you don't

43
00:03:09,166 --> 00:03:12,733
want that software
to close when you run this.

44
00:03:13,033 --> 00:03:14,800
Or you can add your own lines.

45
00:03:14,800 --> 00:03:22,266
Like, copy this line, paste it, change
the name of the application to "cmd.exe".

46
00:03:22,533 --> 00:03:26,633
Do not do that because
this runs in Command Prompt, so I dunno...

47
00:03:26,633 --> 00:03:29,866
And it closes when it's done so I
don't know what would happen at that point.

48
00:03:32,066 --> 00:03:35,066
WMP Network, Windows
Media Player network server.

49
00:03:35,699 --> 00:03:38,966
I'm assuming. Spybot Update Service.

50
00:03:39,666 --> 00:03:46,099
It doesn't update automatically if
you have a version of it that's not paid.

51
00:03:46,333 --> 00:03:46,900
The free version.

52
00:03:47,599 --> 00:03:51,366
iCloudServices.exe. I actually
don't think I need this in here anymore.

53
00:03:51,366 --> 00:03:52,966
I got rid of iCloud.

54
00:03:52,966 --> 00:03:55,966
Uhhm... iCloud.exe, I got rid of it, so.

55
00:03:55,966 --> 00:04:02,400
msosync.exe,
Microsoft Office Sync. I don't use it.

56
00:04:04,433 --> 00:04:07,166
Unchecky gets rid of
checkboxes in installers.

57
00:04:08,400 --> 00:04:17,399
Uh OneNoteM. OneNoteM.exe. Um, I use
OneNote a little. Not that often.

58
00:04:21,300 --> 00:04:24,899
OneNoteM, I'm assuming
that's the Quick Launcher for it, so...

59
00:04:25,466 --> 00:04:26,333
Let's check to make

60
00:04:26,333 --> 00:04:27,333
sure this is...
(Windows XP "Exclamation.wav" sound)

61
00:04:35,000 --> 00:04:37,066
There. Still recording. Okay.

62
00:04:38,033 --> 00:04:39,233
Oh, four minutes. Okay.

63
00:04:42,933 --> 00:04:48,733
OneNoteM.exe. I might
keep this up but just switch to...

64
00:04:48,933 --> 00:04:51,199
Switch to Notepad.

65
00:04:53,666 --> 00:04:56,833
Where was I? Oh yeah,
OneNoteM. That's the OneNote Quick Launcher.

66
00:04:58,066 --> 00:05:00,433
Which isn't... I
don't really use that. If

67
00:05:00,433 --> 00:05:02,766
I wanna open
OneNote, I go to Start Menu,

68
00:05:03,266 --> 00:05:05,699
Type OneNote and then hit enter.

69
00:05:06,733 --> 00:05:08,333
iPodService.exe.

70
00:05:13,266 --> 00:05:20,166
I have no use... uh- the
only reason why I have iTunes

71
00:05:20,166 --> 00:05:24,433
installed on here is
because my mom has an iPhone.

72
00:05:25,266 --> 00:05:30,433
And um, once her
contract is up on that, she's

73
00:05:30,433 --> 00:05:32,266
actually going to
get a Windows Phone instead.

74
00:05:33,166 --> 00:05:35,066
Um, they're, they're really good.

75
00:05:36,699 --> 00:05:43,000
If you don't care about any
applications, buy one. Get a Nokia if possible.

76
00:05:45,966 --> 00:05:48,166
But don't get one
through Verizon. They're awful.

77
00:05:50,633 --> 00:05:56,266
Um, AT&T's okay. I mean, AT&T is really
good with stuff in the US with Windows Phone.

78
00:05:57,366 --> 00:06:02,666
T-Mobile is okay, but try to get
one on AT&T. They're cheaper than Verizon.

79
00:06:04,633 --> 00:06:04,966
(thinking lip smack)

80
00:06:06,199 --> 00:06:10,033
Ehhh... SD Upda- Oh, those are...

81
00:06:11,633 --> 00:06:12,000
(small laugh)

82
00:06:12,366 --> 00:06:14,600
I wonder why I
have both of those in there.

83
00:06:15,199 --> 00:06:17,300
I must have
discovered that in Task Manager.

84
00:06:18,033 --> 00:06:26,366
Task Manager by the way, you can actually see
what applications are running in here.

85
00:06:27,033 --> 00:06:29,133
See, you can see all these, nnn...

86
00:06:30,766 --> 00:06:35,766
And then you can Ctrl-Shift-Escape or
Ctrl-Alt-Delete and then hit Task Manager.

87
00:06:36,866 --> 00:06:38,533
Or on XP, that's...

88
00:06:38,866 --> 00:06:39,533
 (thinking mouth click noise)

89
00:06:39,933 --> 00:06:43,033
On XP, that would be
Ctrl-Alt-Delete and it opens Task Manager.

90
00:06:43,033 --> 00:06:48,199
But Windows 7, probably Vista
and above, I don't use Vista at all.

91
00:06:50,166 --> 00:06:52,833
But yeah, above that, it uses this.

92
00:06:54,333 --> 00:06:56,333
There are some
processes that I don't wanna

93
00:06:56,333 --> 00:06:59,066
close because I don't
know what would happen to it.

94
00:06:59,899 --> 00:07:04,000
Like that one, HP Message Service, and...

95
00:07:05,166 --> 00:07:07,199
(whispering) Aohhhhwhere is it?

96
00:07:08,566 --> 00:07:10,300
HPOSD, On-Screen Display.

97
00:07:11,066 --> 00:07:13,166
I don't wanna
close those and especially

98
00:07:13,166 --> 00:07:15,699
Catalyst Control
Center. That would not be good.

99
00:07:16,000 --> 00:07:18,433
I don't know what would
happen to that if I did that.

100
00:07:18,633 --> 00:07:20,766
Oh, sorry, it's
already 7 minutes. This was

101
00:07:20,766 --> 00:07:24,133
supposed to be a
quick update on my application.

102
00:07:25,133 --> 00:07:26,733
Oh, by the way, if you're on XP...

103
00:07:27,966 --> 00:07:31,433
Sorry if I'm talking too
fast, but if you're on Windows XP,

104
00:07:33,000 --> 00:07:35,966
you probably have heard that
Microsoft dropped support for it.

105
00:07:36,300 --> 00:07:38,066
Aaand...

106
00:07:38,699 --> 00:07:42,399
Dolphin dropped
support for XP and 32-bit Windows.

107
00:07:43,666 --> 00:07:52,566
So you should get a computer with
64-bit and Windows 7 at least if you want.

108
00:07:56,266 --> 00:07:58,133
If you wanna get
the new versions of Dolphin.

109
00:07:59,000 --> 00:08:02,866
They're free, obviously,
but um, they're free applications.

110
00:08:02,866 --> 00:08:05,933
I'm just moving this just so that
there's stuff going on on the screen.

111
00:08:07,566 --> 00:08:12,100
But anyway, or you can get Linux (pronounced as Lienix), you know?

112
00:08:13,000 --> 00:08:15,033
Linux (pronounced as Lienix).
It's available for Linux (pronounced as Lienix). It's a

113
00:08:15,033 --> 00:08:18,866
little confusing, but
it's available for that too, so uhh...

114
00:08:20,000 --> 00:08:25,933
Anyway, sdupdate.exe, vmware-authd.exe. I don't know what that does.

115
00:08:26,000 --> 00:08:30,766
I have VMware on my computer.
I'm gonna assume that it's a VMware (voice
goes up toward the end like exxagerated uptalk)...

116
00:08:30,899 --> 00:08:34,866
I'm gonna assume
that it's a VMware process.

117
00:08:35,833 --> 00:08:39,633
Authorization. I'm
going to assume it's a daemon

118
00:08:39,633 --> 00:08:44,566
of some sort that
stores your stuff or something.

119
00:08:45,466 --> 00:08:49,799
Opera_AutoUpdate.exe, I
don't really use Opera much anymore.

120
00:08:50,033 --> 00:08:56,899
I used Opera 12.16 just
because it's not Chrome, because I

121
00:08:56,899 --> 00:09:01,233
didn't like Google for a
while, because of spying and stuff.

122
00:09:01,433 --> 00:09:03,100
But I have add-ons for that.

123
00:09:05,333 --> 00:09:11,666
AppleMobileDeviceService.exe. Oh, and
by the way, iTunes is so awful, when it opens,

124
00:09:12,000 --> 00:09:16,799
it takes forever,
five minutes or so to get from

125
00:09:16,799 --> 00:09:23,433
actually fully opening
to the iPhone and backup screen.

126
00:09:24,466 --> 00:09:28,133
Yeah. Launcher.exe,
that's an Opera application.

127
00:09:28,466 --> 00:09:44,033
So... if you wanna make your own one,
say like right-click, copy, enter, paste, app.exe.

128
00:09:44,466 --> 00:09:48,066
I don't think I have anything
running on here that's app.exe though.

129
00:09:51,333 --> 00:09:57,066
You can just do this. Oh, and by the
way, if you're running Linux 
(pronounced as Lienix), this won't work

130
00:09:57,066 --> 00:09:59,666
because it's a
.bat file and it's Windows.

131
00:10:01,466 --> 00:10:09,966
I'm pretty sure that ah, well, you can
take out any of these if you use iTunes.

132
00:10:10,033 --> 00:10:23,733
Sorry about what I said with that,
but if you use iTunes or iCloudServices or ummm... SD

133
00:10:23,733 --> 00:10:29,966
Update, if you have any of those
running and you need those, take that line out.

134
00:10:30,000 --> 00:10:33,266
Just take that line out and then save it.

135
00:10:35,500 --> 00:10:38,166
And you can add whatever you want to it.

136
00:10:38,166 --> 00:10:39,566
And um...

137
00:10:41,233 --> 00:10:50,399
I am going to open this actually
in the command prompt. Can I pause this?

138
00:10:52,100 --> 00:10:53,133
Can I...

139
00:10:53,133 --> 00:10:55,100
Pause this...

140
00:10:56,533 --> 00:10:57,466
I don't know. Just-

141
00:10:57,466 --> 00:11:00,266
Now, if you did not
hear me, see now it's going to go through the

142
00:11:00,266 --> 00:11:04,899
through all the things
and try to stop the processes...

143
00:11:06,266 --> 00:11:13,433
if anything. See
these that stopped some of 'em.

144
00:11:15,299 --> 00:11:20,566
But, oh, Apple Mobile Device
Service is running. See that, for me personally

145
00:11:20,566 --> 00:11:25,233
though, that application is
useless. For me anyway, for my uses.

146
00:11:26,600 --> 00:11:35,233
So, I have updated it
on my Google Drive and in the

147
00:11:35,233 --> 00:11:39,700
description you will find a
link to that version of the application.

148
00:11:40,033 --> 00:11:45,933
Well, batch file actually. And if
you didn't catch what I said about the

149
00:11:45,933 --> 00:11:50,733
administrative privileges,
I set it to have administrative

150
00:11:50,733 --> 00:11:55,899
priv- bleagh! privileges so that it
would close some of the applications.

151
00:11:56,899 --> 00:12:03,133
Like, ohhh, Apple Mobile Device Service.exe I think, and a few others, otherwise it

152
00:12:03,133 --> 00:12:05,966
wouldn't work.
Otherwise it wouldn't close those.

153
00:12:08,333 --> 00:12:11,633
So it just, when I double
click on it, it just asks me for

154
00:12:11,633 --> 00:12:14,333
my password. Actually
the administrative password.

155
00:12:15,333 --> 00:12:23,066
So, yeah! And
please leave any questions that

156
00:12:23,066 --> 00:12:25,700
you might have in the
comments section down below.

157
00:12:26,366 --> 00:12:32,299
And, yeah, this is my
first video of 2015. (slight whisper) Yeah... I

158
00:12:33,533 --> 00:12:37,166
I just wanted to do a
video of some sort, so goodbye.

